@extends('welcome')

@section('title')
    <title>Shopworks Rotas</title>
@stop

@section('content')
    <!-- a dirty way of getting all the unique users without making another database call -->
    <?php $users = (array_unique((array) $rota->pluck('staffid')->toArray())); ?>
    <h1>Shopworks Rotas</h1>

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><b class="text-info">Weekly Rota</b></div>

        <!-- Table -->
        <table class="table">
            <thead>
                <tr>
                    <th class="text-warning">Name</th>
                    <th class="text-danger">Monday</th>
                    <th class="text-danger">Tuesday</th>
                    <th class="text-danger">Wednesday</th>
                    <th class="text-danger">Thursday</th>
                    <th class="text-danger">Friday</th>
                    <th class="text-danger">Saturday</th>
                    <th class="text-danger">Sunday</th>
                </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">Employee {{ $user }}</th>
                    <!-- a dirty way of quickly figuring out do we have the right entry in our collection -->
                    @for($day = 0;$day<7;$day++)
                        <?php $filtered = $rota->filter(function ($rotaitem) use ($user,$day) {
                            return $rotaitem->staffid == $user;
                        })->filter(function($rotauseritem) use ($user,$day) {
                            return $rotauseritem->daynumber == $day;
                        }); ?>
                        @if(count($filtered)>0)
                            <td>{{ $filtered->first()['starttime'] }} - {{ $filtered->first()['endtime'] }}</td>
                        @else
                            <td>OFF</td>
                        @endif
                    @endfor
                </tr>
            @endforeach
                <tr>
                    <th scope="row">Total Hours</th>
                    @foreach($dailyTotalHours as $data)
                        <td>{{ $data }}</td>
                    @endforeach
                </tr>
                <tr>
                    <th scope="row">Minutes Alone</th>
                    @foreach($dailyHoursAlone as $data)
                        <td>{{ $data }}</td>
                    @endforeach
                </tr>
            </tbody>
        </table>
    </div>
    @foreach($rota as $r)

    @endforeach

@stop