<?php

namespace Shopworks\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Shopworks\Calculator\HourCalculator\HourCalculator;
use Shopworks\Persistence\RotaRepository;
use Shopworks\Calculator\AloneCalculator\AloneCalculator;


class RotaController extends Controller
{
    /**
     * @var RotaRepository
     */
    private $rotaRepository;
    /**
     * @var HourCalculator
     */
    private $hourCalculator;
    /**
     * @var AloneCalculator
     */
    private $aloneCalculator;

    /**
     * RotaController constructor.
     * @param RotaRepository $rotaRepository
     * @param HourCalculator $hourCalculator
     * @param AloneCalculator $aloneCalculator
     */
    public function __construct(RotaRepository $rotaRepository, HourCalculator $hourCalculator,AloneCalculator $aloneCalculator)
    {
        $this->rotaRepository = $rotaRepository;
        $this->hourCalculator = $hourCalculator;
        $this->aloneCalculator = $aloneCalculator;
    }

    /**
     * @return array
     */
    public function index()
    {
        $rotaCollection = $this->rotaRepository->getFullRota();
        $dailyTotalHours = $this->hourCalculator->calculateForRota($rotaCollection);
        $dailyHoursAlone = $this->aloneCalculator->calculateForRota($rotaCollection);

        return [$dailyTotalHours, $dailyHoursAlone];
        //        return view('rotas')->with(compact('rota','dailyTotalHours','dailyHoursAlone'));
    }
}
