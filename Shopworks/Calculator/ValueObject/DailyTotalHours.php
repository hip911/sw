<?php

namespace Shopworks\Calculator\ValueObject;

use Shopworks\Persistence\Rota;

class DailyTotalHours
{
    /**
     * @var array
     */
    private $dailyTotalHours = [
        '0'=>0.00,
        '1'=>0.00,
        '2'=>0.00,
        '3'=>0.00,
        '4'=>0.00,
        '5'=>0.00,
        '6'=>0.00
    ];

    /**
     * DailyTotalHours constructor.
     * @param array $override
     */
    public function __construct(array $override = [])
    {
        foreach ($override as $dayNumber => $totalHoursOnDay) {
            if (array_key_exists($dayNumber, $this->dailyTotalHours)) {
                $this->dailyTotalHours[$dayNumber] = $totalHoursOnDay;
            }
        }
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return $this->dailyTotalHours;
    }

    /**
     * @param $daynumber
     * @param $hours
     * @return static
     */
    public function addHoursForDaynumber($daynumber, $hours)
    {
        $tempDailyTotalHours = $this->dailyTotalHours;
        $tempDailyTotalHours[$daynumber] += $hours;

        return new static($tempDailyTotalHours);
    }

    /**
     * @param Rota $rota
     * @return static
     */
    public function addHoursForRota(Rota $rota)
    {
        $tempDailyTotalHours = $this->dailyTotalHours;
        $tempDailyTotalHours[$rota->daynumber] += $rota->workhours;

        return new static($tempDailyTotalHours);
    }
}
