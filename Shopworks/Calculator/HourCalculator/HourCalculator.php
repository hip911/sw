<?php

namespace Shopworks\Calculator\HourCalculator;

use Illuminate\Database\Eloquent\Collection;
use Shopworks\Calculator\ValueObject\DailyTotalHours;
use Shopworks\Persistence\Rota;

class HourCalculator
{
    private $dailyTotalHours;

    public function __construct(DailyTotalHours $dailyTotalHours)
    {
        $this->dailyTotalHours = $dailyTotalHours;
    }

    public function calculateForRota(Collection $rotaCollection)
    {
        foreach ($rotaCollection as $rotaItem) {
            if (!is_a($rotaItem,Rota::class)) {
                throw new \RuntimeException('Unknown item passed to '. __CLASS__);
            }
            $this->dailyTotalHours = $this->dailyTotalHours->addHoursForRota($rotaItem);
        }

        return $this->dailyTotalHours;
    }
}
