<?php

namespace Shopworks\Calculator\AloneCalculator;

use Illuminate\Database\Eloquent\Collection;

class DailyAloneCalculator
{
    const SECONDS_IN_HOUR = 3600;

    /**
     * @var TimestampCollector
     */
    private $timestampCollector;
    /**
     * @var AloneChecker
     */
    private $aloneChecker;

    /**
     * DailyAloneCalculator constructor.
     * @param TimestampCollector $timestampCollector
     * @param AloneChecker $aloneChecker
     */
    public function __construct(TimestampCollector $timestampCollector, AloneChecker $aloneChecker)
    {
        $this->timestampCollector = $timestampCollector;
        $this->aloneChecker = $aloneChecker;
    }

    /**
     * The daily calculation. Firstly get all timestamps for any start/end.
     * Take an immediate moment (+1sec) for each period between two timestamps (ordered on collection),get the occurrences in all the different shift entries.
     * If it is one, add that period quickly to our sum.
     *
     * @param Collection $shifts
     * @return float|int
     */
    public function calculateHoursPerDay(Collection $shifts)
    {
        $shiftBorders = $this->timestampCollector->collectTimestamps($shifts);
        $hoursAloneOneDay = 0;
        for($i = 0; $i < count($shiftBorders) - 1; $i++) {
            if($this->aloneChecker->checkIfAlone(($shiftBorders[$i] + 1), $shifts)) {
                $hoursAloneOneDay += ($shiftBorders[$i+1] - $shiftBorders[$i]) / self::SECONDS_IN_HOUR;
            }
        }

        return $hoursAloneOneDay;
    }
}
