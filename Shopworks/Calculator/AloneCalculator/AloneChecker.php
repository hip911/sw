<?php

namespace Shopworks\Calculator\AloneCalculator;

class AloneChecker
{
    /**
     * @param $timestamp
     * @param $shifts
     * @return bool
     */
    public function checkIfAlone($timestamp, $shifts)
    {
        $count = 0;
        foreach($shifts as $shift) {
            if($timestamp > $shift->starttime && $timestamp < $shift->endtime) {
                $count++;
            }
        }

        return $count == 1;
    }
}
