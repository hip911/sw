<?php

namespace Shopworks\Calculator\AloneCalculator;

use Illuminate\Database\Eloquent\Collection;
use Shopworks\Calculator\ValueObject\DailyTotalHours;

class AloneCalculator
{
    const MON = '0';
    const TUE = '1';
    const WED = '2';
    const THU = '3';
    const FRI = '4';
    const SAT = '5';
    const SUN = '6';

    private $dailyTotalHours;
    private $dailyAloneCalculator;

    private static $days = [
        self::MON,
        self::TUE,
        self::WED,
        self::THU,
        self::FRI,
        self::SAT,
        self::SUN
    ];


    public function __construct(DailyTotalHours $dailyTotalHours, DailyAloneCalculator $dailyAloneCalculator)
    {
        $this->dailyTotalHours = $dailyTotalHours;
        $this->dailyAloneCalculator = $dailyAloneCalculator;
    }

    public function calculateForRota(Collection $rotaCollection)
    {
        $rotaCollection = $rotaCollection->map(function($item){
                $item->starttime = $this->convertToTimestamp($item->starttime);
                $item->endtime = $this->convertToTimestamp($item->endtime);

                return $item;
        });

        $grouped = $rotaCollection->groupBy(function ($item, $key) {
            return $item->daynumber;
        });

        foreach(self::$days as $dayNumber) {
            if(isset($grouped[$dayNumber])) {
                $hoursPerDay = $this->dailyAloneCalculator->calculateHoursPerDay($grouped[$dayNumber]);
                $this->dailyTotalHours = $this->dailyTotalHours->addHoursForDaynumber($dayNumber, $hoursPerDay);
            }
        }

        return $this->dailyTotalHours;
    }

    /*
     * Quickfix to work with the times coming from the database.
     * If a shift can start before 10:00am, needs to be changed.
     */

    private function convertToTimestamp($time)
    {
        return ($time[0] === '0') ? strtotime($time)+86400 : strtotime($time);
    }
}