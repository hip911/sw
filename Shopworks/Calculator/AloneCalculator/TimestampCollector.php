<?php

namespace Shopworks\Calculator\AloneCalculator;

use Illuminate\Database\Eloquent\Collection;

class TimestampCollector
{
    /**
     * Collect all unique timestamps for start/end for a certain day, then return it sorted
     *
     * @param Collection $shifts
     * @return array
     */
    public function collectTimestamps(Collection $shifts)
    {
        $timestamps = [];
        foreach($shifts as $shift) {
            foreach ([$shift->starttime, $shift->endtime] as $timestamp) {
                if(!in_array($timestamp, $timestamps)) {
                    $timestamps[] = $timestamp;
                }
            }
        }
        sort($timestamps);

        return $timestamps;
    }
}