<?php namespace Shopworks\Persistence;


class RotaRepository {

    private $model;

    public function __construct(Rota $model)
    {
        $this->model = $model;
    }

    public function getFullRota()
    {
        return $this->model->HasStaffId()->TypeShift()->orderBy('daynumber','ASC')->orderBy('starttime','ASC')->get();
    }
}

