<?php

namespace Shopworks\Persistence;

use Illuminate\Database\Eloquent\Model;

class Rota extends Model
{
    protected $table = 'rota_slot_staff';

    public $timestamps = false;

    protected $fillable = ['id','rotaid','daynumber','staffid','slottype','starttime','endtime','workhours','premiumminutes','roletypeid','freeminutes','seniorcashierminutes','splitshifttimes'];

    public function scopeHasStaffId($q)
    {
        return $q->whereNotNull('staffid');
    }

    public function scopeTypeShift($q)
    {
        return $q->where('slottype','=','shift');
    }
}
