<?php

namespace spec\Shopworks\Calculator\AloneCalculator;

use Illuminate\Database\Eloquent\Collection;
use Prophecy\Argument;
use Shopworks\Calculator\AloneCalculator\AloneChecker;
use Shopworks\Calculator\AloneCalculator\TimestampCollector;
use spec\Shopworks\BaseSpec;

class DailyAloneCalculatorSpec extends BaseSpec
{
    private $timestampCollector;
    private $aloneChecker;

    function let(TimestampCollector $timestampCollector, AloneChecker $aloneChecker)
    {
        $this->timestampCollector = $timestampCollector;
        $this->aloneChecker = $aloneChecker;
        $this->beConstructedWith($this->timestampCollector, $this->aloneChecker);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Shopworks\Calculator\AloneCalculator\DailyAloneCalculator');
    }

    function it_summarizes_the_hours_when_a_single_staff_was_present_in_the_shop(Collection $rotaCollection)
    {
        $this->timestampCollector->collectTimestamps(Argument::any())->willReturn(
            array_map(function($item){
                return ($item[0] === '0') ? strtotime($item)+86400 : strtotime($item);
            },['10:00:00','11:00:00','16:00:00','19:00:00','00:45:00','03:00:00']
            )
        );
        $this->aloneChecker->checkIfAlone(Argument::cetera())->willReturn(true,false,true,false,true,false);

        $this->calculateHoursPerDay($rotaCollection)->shouldReturn(6.25);
    }
}
