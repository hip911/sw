<?php

namespace spec\Shopworks\Calculator\AloneCalculator;

use Illuminate\Database\Eloquent\Collection;
use Shopworks\Calculator\AloneCalculator\AloneChecker;
use spec\Shopworks\BaseSpec;

class AloneCheckerSpec extends BaseSpec
{
    protected $rotaCollection;

    function let()
    {
        $this->rotaCollection = new Collection([
            $this->getFakeRota(['starttime'=>strtotime('18:00:00'),'endtime'=>strtotime('20:00:00'),'workhours'=>8.00]),
            $this->getFakeRota(['starttime'=>strtotime('19:00:00'),'endtime'=>strtotime('03:00:00')+86400,'workhours'=>8.00]),
            $this->getFakeRota(['starttime'=>strtotime('19:00:00'),'endtime'=>strtotime('01:00:00')+86400,'workhours'=>6.00])
        ]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(AloneChecker::class);
    }

    function it_returns_false_if_there_is_no_staff_in_the_store()
    {
        $this->checkIfAlone(strtotime('10:00:00'),$this->rotaCollection)->shouldReturn(false);
    }

    function it_returns_false_if_there_are_multiple_staff_in_the_store()
    {
        $this->checkIfAlone(strtotime('19:30:00'),$this->rotaCollection)->shouldReturn(false);
    }

    function it_returns_true_if_there_is_a_single_staff_in_the_store()
    {
        $this->checkIfAlone(strtotime('01:00:00')+86400,$this->rotaCollection)->shouldReturn(true);
    }
}
