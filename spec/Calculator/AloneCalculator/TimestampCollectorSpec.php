<?php

namespace spec\Shopworks\Calculator\AloneCalculator;

use Illuminate\Database\Eloquent\Collection;
use Shopworks\Calculator\AloneCalculator\TimestampCollector;
use spec\Shopworks\BaseSpec;

class TimestampCollectorSpec extends BaseSpec
{
    private $rotaCollection;

    /** @mixin TimestampCollector */
    function let()
    {
        $this->rotaCollection = new Collection([
            $this->getFakeRota(['starttime'=>strtotime('19:00:00'),'endtime'=>strtotime('03:00:00')+86400,'workhours'=>6.00]),
            $this->getFakeRota(['starttime'=>strtotime('19:00:00'),'endtime'=>strtotime('01:00:00')+86400,'workhours'=>6.00])
        ]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Shopworks\Calculator\AloneCalculator\TimestampCollector');
    }

    function it_collects_all_unique_timestamp_from_the_collection_of_shifts()
    {
        $this->collectTimestamps($this->rotaCollection)->shouldReturn(
            array_map(function($item){
                return ($item[0] === '0') ? strtotime($item)+86400 : strtotime($item);
            },['19:00:00','01:00:00','03:00:00']
            )
        );
    }
}
