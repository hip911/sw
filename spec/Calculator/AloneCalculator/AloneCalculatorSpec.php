<?php

namespace spec\Shopworks\Calculator\AloneCalculator;

use Illuminate\Database\Eloquent\Collection;
use Prophecy\Argument;
use Shopworks\Calculator\AloneCalculator\DailyAloneCalculator;
use Shopworks\Calculator\ValueObject\DailyTotalHours;
use spec\Shopworks\BaseSpec;

class AloneCalculatorSpec extends BaseSpec
{
    private $rotaCollection;
    private $dac;

    function let(DailyAloneCalculator $dailyAloneCalculator)
    {
        $dailyTotalHours = new DailyTotalHours();
        $this->dac = $dailyAloneCalculator;
        $this->rotaCollection = new Collection([
            $this->getFakeRota(['starttime'=>'15:00:00','endtime'=>'17:00:00','workhours'=>2.00]),
            $this->getFakeRota(['starttime'=>'16:00:00','endtime'=>'19:00:00','workhours'=>3.00]),
            $this->getFakeRota(),
            $this->getFakeRota(['starttime'=>'19:00:00','endtime'=>'00:45:00','workhours'=>5.75]),
            $this->getFakeRota(['daynumber'=>2,'starttime'=>'19:00:00','endtime'=>'03:00:00','workhours'=>8.00]),
            $this->getFakeRota(['daynumber'=>2,'starttime'=>'19:00:00','endtime'=>'00:45:00','workhours'=>5.75])
        ]);

        $this->beConstructedWith($dailyTotalHours, $dailyAloneCalculator);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Shopworks\Calculator\AloneCalculator\AloneCalculator');
    }

    function it_calculates_the_time_alone_per_day_for_a_collection_of_shifts()
    {
        $this->dac->calculateHoursPerDay(Argument::any())->willReturn(5.25,2.25);

        $result = $this->calculateForRota($this->rotaCollection);
        $result->shouldBeAnInstanceOf(DailyTotalHours::class);
        $result->getArrayCopy()->shouldReturn( (new DailyTotalHours(['1'=>5.25,'2'=>2.25]))->getArrayCopy());
    }
}
