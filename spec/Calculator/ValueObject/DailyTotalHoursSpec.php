<?php

namespace spec\Shopworks\Calculator\ValueObject;

use Shopworks\Calculator\ValueObject\DailyTotalHours;
use spec\Shopworks\BaseSpec;

class DailyTotalHoursSpec extends BaseSpec
{
    /** @mixin DailyTotalHours */

    function it_is_initializable()
    {
        $this->shouldHaveType('Shopworks\Calculator\ValueObject\DailyTotalHours');
    }

    function it_has_all_zeros_for_all_daynumbers_on_initialization_with_no_overrides()
    {
        $this->getArrayCopy()->shouldReturn([
            '0'=>0.00,
            '1'=>0.00,
            '2'=>0.00,
            '3'=>0.00,
            '4'=>0.00,
            '5'=>0.00,
            '6'=>0.00
        ]);
    }

    function it_has_ability_to_override_default_values()
    {
        $this->beConstructedWith([
            '0'=>1.00,
            '1'=>2.00,
            '2'=>3.00,
            '3'=>4.00,
            '4'=>5.00,
            '5'=>6.00,
            '6'=>7.00,
            'somethingNonExisting' => 'hellothere'
        ]);

        $this->getArrayCopy()->shouldReturn([
            '0'=>1.00,
            '1'=>2.00,
            '2'=>3.00,
            '3'=>4.00,
            '4'=>5.00,
            '5'=>6.00,
            '6'=>7.00
        ]);
    }

    function it_allows_to_add_hours_for_a_certain_day_of_the_week()
    {
        $this->beConstructedWith([
            '0'=>1.00,
            '1'=>2.00,
            '2'=>3.00,
            '3'=>4.00,
            '4'=>5.00,
            '5'=>6.00,
            '6'=>7.00,
        ]);

        $fakeRota = $this->getFakeRota(['daynumber'=>6]);

        $updatedDailyTotalHours = $this->addHoursForRota($fakeRota);
        $updatedDailyTotalHours = $updatedDailyTotalHours->addHoursForDaynumber('2',3);

        $updatedDailyTotalHours->getArrayCopy()->shouldReturn([
            '0'=>1.00,
            '1'=>2.00,
            '2'=>6.00,
            '3'=>4.00,
            '4'=>5.00,
            '5'=>6.00,
            '6'=>15.00,
        ]);
    }
}
