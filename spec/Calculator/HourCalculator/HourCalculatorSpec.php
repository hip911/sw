<?php

namespace spec\Shopworks\Calculator\HourCalculator;

use Illuminate\Database\Eloquent\Collection;
use Shopworks\Calculator\ValueObject\DailyTotalHours;
use Shopworks\Calculator\HourCalculator\HourCalculator;
use spec\Shopworks\BaseSpec;

class HourCalculatorSpec extends BaseSpec
{
    /** @mixin HourCalculator */

    private $dailyTotalHours;
    private $rotaCollection;

    function let(){
        $this->dailyTotalHours = new DailyTotalHours();
        $this->rotaCollection = new Collection([
            $this->getFakeRota(),
            $this->getFakeRota(['endtime'=>'01:00:00','workhours'=>6.00])

        ]);
        $this->beConstructedWith($this->dailyTotalHours);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Shopworks\Calculator\HourCalculator\HourCalculator');
    }

    function it_adds_up_the_days_from_the_rota()
    {
        $result = $this->calculateForRota($this->rotaCollection);
        $result->shouldBeAnInstanceOf(DailyTotalHours::class);
        $result->getArrayCopy()->shouldReturn( (new DailyTotalHours(['1'=>14.0]))->getArrayCopy());
    }

    function it_throws_when_a_dirty_collection_is_passed_for_calculation()
    {
        $dirtyRotaCollection = new Collection([
            new Collection([])
        ]);

        $this->shouldThrow(\RuntimeException::class)->during('calculateForRota',[$dirtyRotaCollection]);

    }
}
