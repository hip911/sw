<?php

namespace spec\Shopworks;

use PhpSpec\ObjectBehavior;
use Shopworks\Persistence\Rota;

class BaseSpec extends ObjectBehavior
{
    private static $defaultParams = [
        'daynumber'=>1,
        'staffid'=>null,
        'slottype'=>'shift',
        'starttime'=>'19:00:00',
        'endtime'=>'03:00:00',
        'workhours'=>8.00,
        'premiumminutes'=>0,
        'roletypeid'=>11,
        'freeminutes'=>0,
        'seniorcashierminutes'=>0,
        'splitshifttimes'=>0
    ];

    function getFakeRota(array $overrides = []) {
        $params = self::$defaultParams;
        foreach ($overrides as $key => $value) {
            if(!array_key_exists($key,self::$defaultParams)) {
                continue;
            }
            $params[$key] = $value;
        }

        return new Rota($params);
    }
}
