<?php

use Illuminate\Database\Eloquent\Collection;
use Shopworks\Calculator\AloneCalculator\AloneCalculator;
use Shopworks\Calculator\AloneCalculator\AloneChecker;
use Shopworks\Calculator\AloneCalculator\DailyAloneCalculator;
use Shopworks\Calculator\AloneCalculator\TimestampCollector;
use Shopworks\Calculator\HourCalculator\HourCalculator;
use Shopworks\Calculator\ValueObject\DailyTotalHours;
use Shopworks\Controllers\RotaController;
use Shopworks\Persistence\Rota;
use Shopworks\Persistence\RotaRepository;

class RotaControllerTest extends \PHPUnit_Framework_TestCase {

    private $rotaController;

    public function setUp()
    {
        $mockedCollection = new Collection($this->getMockRotaData());

        /** @var RotaRepository $mockRotaRepository */
        $mockRotaRepository = $this->prophesize(RotaRepository::class);
        $mockRotaRepository->getFullRota()->willReturn($mockedCollection);
        $this->rotaController = new RotaController(
            $mockRotaRepository->reveal(),
            new HourCalculator(new DailyTotalHours()),
            new AloneCalculator(new DailyTotalHours(),new DailyAloneCalculator(new TimestampCollector(),new AloneChecker()))
        );
    }

    public function test_it_captures_hourcalculator_functionality()
    {
        $calculations = $this->rotaController->index();
        $expected = ['0'=>0.00,'1'=>10.00,'2'=>0.00,'3'=>0.00,'4'=>0.00,'5'=>0.00,'6'=>24.25];
        $this->assertEquals($expected,$calculations[0]->getArrayCopy());
    }

    public function test_it_captures_alonecalculator_functionality()
    {
        $calculations = $this->rotaController->index();
        $expected = ['0'=>0.00,'1'=>10.00,'2'=>0.00,'3'=>0.00,'4'=>0.00,'5'=>0.00,'6'=>8.75];
        $this->assertEquals($expected,$calculations[1]->getArrayCopy());
    }

    private function getMockRotaData() {
        return [
            new Rota([
                'daynumber'=>6,
                'staffid'=>null,
                'slottype'=>'shift',
                'starttime'=>'19:00:00',
                'endtime'=>'03:00:00',
                'workhours'=>8.00,
                'premiumminutes'=>0,
                'roletypeid'=>11,
                'freeminutes'=>0,
                'seniorcashierminutes'=>0,
                'splitshifttimes'=>0
            ]),
            new Rota([
                'daynumber'=>6,
                'staffid'=>null,
                'slottype'=>'shift',
                'starttime'=>'19:00:00',
                'endtime'=>'01:00:00',
                'workhours'=>6.00,
                'premiumminutes'=>0,
                'roletypeid'=>11,
                'freeminutes'=>0,
                'seniorcashierminutes'=>0,
                'splitshifttimes'=>0
            ]),
            new Rota([
                'daynumber'=>6,
                'staffid'=>null,
                'slottype'=>'shift',
                'starttime'=>'11:00:00',
                'endtime'=>'20:00:00',
                'workhours'=>9.00,
                'premiumminutes'=>0,
                'roletypeid'=>11,
                'freeminutes'=>0,
                'seniorcashierminutes'=>0,
                'splitshifttimes'=>0
            ]),
            new Rota([
                'daynumber'=>1,
                'staffid'=>null,
                'slottype'=>'shift',
                'starttime'=>'11:00:00',
                'endtime'=>'20:00:00',
                'workhours'=>9.00,
                'premiumminutes'=>0,
                'roletypeid'=>11,
                'freeminutes'=>0,
                'seniorcashierminutes'=>0,
                'splitshifttimes'=>0
            ]),
            new Rota([
                'daynumber'=>1,
                'staffid'=>null,
                'slottype'=>'shift',
                'starttime'=>'20:00:00',
                'endtime'=>'21:00:00',
                'workhours'=>1.00,
                'premiumminutes'=>0,
                'roletypeid'=>11,
                'freeminutes'=>0,
                'seniorcashierminutes'=>0,
                'splitshifttimes'=>0
            ]),
            new Rota([
                'daynumber'=>6,
                'staffid'=>null,
                'slottype'=>'shift',
                'starttime'=>'14:30:00',
                'endtime'=>'15:45:00',
                'workhours'=>1.25,
                'premiumminutes'=>0,
                'roletypeid'=>11,
                'freeminutes'=>0,
                'seniorcashierminutes'=>0,
                'splitshifttimes'=>0
            ])
        ];
    }
}